package main;

public class BreakfastMenu implements Cafe{

    @Override
    public String viewMenu(){
        return "Вам принесли меню завтраков";
    }

    @Override
    public void visit(Visitor visitor) {
        visitor.breakfastMenu(this);
    }
}
