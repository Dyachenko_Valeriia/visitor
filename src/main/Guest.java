package main;

public class Guest implements Visitor{
    @Override
    public String breakfastMenu(BreakfastMenu breakfastMenu) {
        return breakfastMenu.viewMenu();
    }

    @Override
    public String businessLunchMenu(BusinessLunchMenu businessLunchMenu) {
        return businessLunchMenu.viewMenu();
    }

    @Override
    public String menu(Menu menu) {
        return menu.viewMenu();
    }
}
