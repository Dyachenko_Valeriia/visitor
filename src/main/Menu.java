package main;

public class Menu implements Cafe{

    public String viewMenu(){
        return "Вам принесли обычное меню";
    }

    @Override
    public void visit(Visitor visitor) {
        visitor.menu(this);
    }
}
