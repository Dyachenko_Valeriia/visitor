package main;

public interface Cafe {
    void visit(Visitor visitor);
    String viewMenu();
}
