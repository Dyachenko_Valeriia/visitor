package main;

public class BusinessLunchMenu implements Cafe{
    public String viewMenu(){
        return "Вам принесли бизнес-ланч меню";
    }

    @Override
    public void visit(Visitor visitor) {
        visitor.businessLunchMenu(this);
    }
}
