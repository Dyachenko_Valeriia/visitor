package main;

public interface Visitor {
    public String breakfastMenu(BreakfastMenu  breakfastMenu);
    public String businessLunchMenu(BusinessLunchMenu  businessLunchMenu);
    public String menu(Menu menu);
}
