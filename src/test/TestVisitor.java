package test;

import main.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestVisitor {
    @Test
    public void testBreakfastMenu() {
        Cafe cafe = new BreakfastMenu();
        cafe.visit(new Guest());
        assertEquals("Вам принесли меню завтраков", cafe.viewMenu());
    }

    @Test
    public void testBusinessLunchMenu() {
        Cafe cafe = new BusinessLunchMenu();
        cafe.visit(new Guest());
        assertEquals("Вам принесли бизнес-ланч меню", cafe.viewMenu());
    }

    @Test
    public void testMenu() {
        Cafe cafe = new Menu();
        cafe.visit(new Guest());
        assertEquals("Вам принесли обычное меню", cafe.viewMenu());
    }
}
